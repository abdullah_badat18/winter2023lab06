public class Board{
	
	private Die die1;
	private Die die2;
	private boolean[] tiles;
	
	public Board(){
		this.die1 = new Die();
		this.die2 = new Die();
		this.tiles = new boolean[12];
	}
	
	public String toString(){
		String board = "";
		
		for(int i=0; i<tiles.length; i++)
		{
			if((tiles[i]))
			{
				board =  board+ " " +"X";
			}
			else
			{
				board = board+ " " + (i+1);
			}
		}
		return board;
	}
	
	public boolean playATurn()
	{
		this.die1.roll();
		this.die2.roll();
		System.out.println(die1.getFaceValue());	
		System.out.println(die2.getFaceValue());
		
		int sumOfDices = die1.getFaceValue() + die2.getFaceValue();
		
			if(!(this.tiles[sumOfDices-1]))
			{
				this.tiles[sumOfDices-1] = true;
				System.out.println("Closing tile equal to sum: "+ sumOfDices);
				return false;
			}
			if(!(this.tiles[this.die1.getFaceValue()-1]))
			{
				this.tiles[this.die1.getFaceValue()-1] = true;
				System.out.println("Closing tile with the same value as die one: "+this.die1.getFaceValue());
				return false;
			}
			if(!(this.tiles[this.die2.getFaceValue()-1]))
			{
				this.tiles[this.die2.getFaceValue()-1] = true;
				System.out.println("Closing tile with the same value as die one: "+this.die2.getFaceValue());
				return false;
			}
			
			System.out.println("All the tiles for these values are already shut");
			return true;
	}
}

