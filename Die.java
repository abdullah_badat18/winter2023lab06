import java.util.Random;
public class Die{
	
	
	private int faceValue;
	private Random generator;
	
	public Die(){
		this.faceValue = 1;
		this.generator = new Random();	
	}

	public int getFaceValue(){
		return this.faceValue;
	}
	
	public void roll()
	{
		this.faceValue = generator.nextInt(6)+1;
	}
	
	public String toString(){
		return "number rolled: " +this.faceValue;
	}
}
