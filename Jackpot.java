public class Jackpot{

public static void main(String[] args)
	{
		System.out.println(" Welcome! Let's Start the Game of Jackpot - A board Game With Dice and Wooden Tiles.");
		
		Die die = new Die();
		die.roll();
		System.out.println(die);
		
		Board board = new Board();
		
		boolean gameOver = false;
		
		int numOfTilesClosed = 0;
		
		while(!(gameOver))
		{
			System.out.println(board);
			
			if(board.playATurn())
			{
				gameOver = true;
			}
			else
			{	
				numOfTilesClosed++;
			}
		}
		
		if(numOfTilesClosed >= 7)
		{
			System.out.println("You Have Reached The Jackpot and Won!!");
		}
		else
		{
			System.out.println("You Lost...");
		}
	}
}